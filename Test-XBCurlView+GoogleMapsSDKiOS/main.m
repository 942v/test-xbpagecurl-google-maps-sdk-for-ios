//
//  main.m
//  Test-XBCurlView+GoogleMapsSDKiOS
//
//  Created by Guillermo Saenz on 5/15/13.
//  Copyright (c) 2013 LaHacemos. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

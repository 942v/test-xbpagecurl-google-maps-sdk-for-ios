//
//  ViewController.m
//  Test-XBCurlView+GoogleMapsSDKiOS
//
//  Created by Guillermo Saenz on 5/15/13.
//  Copyright (c) 2013 LaHacemos. All rights reserved.
//

#import "ViewController.h"

#import <GoogleMaps/GoogleMaps.h>

#define kDuration 0.3f
#define rCurlPortrait 150.0f
#define rCurlLandscape 80.0f

@implementation ViewController{
    GMSMapView *googleMapView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Setup of the snap point (XBPageDragView)
    snapPoint = [[XBSnappingPoint alloc] initWithPosition:CGPointMake(self.pageDragView.viewToCurl.frame.size.width*0.5, self.pageDragView.viewToCurl.frame.size.height*0.4) angle:7*M_PI/8 radius:rCurlPortrait weight:0.5];
    
    [self.pageDragView.pageCurlView addSnappingPoint:snapPoint];
    
    // Setting Google Map
    // Lima - Peru. My city - country
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:-12.044207 longitude:-77.061459 zoom:10];
    googleMapView = [GMSMapView mapWithFrame:frontView.bounds camera:camera];
    
    googleMapView.myLocationEnabled = YES;
    
    [googleMapView setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
    googleMapView.delegate = self;
    
    googleMapView.settings.compassButton = YES;
    
    [frontView addSubview:googleMapView];
}

- (void)viewDidAppear:(BOOL)animated{
    [self.pageDragView refreshPageCurlView];
}

- (void)viewWillAppear:(BOOL)animated{
    [googleMapView startRendering];
}

- (void)viewWillDisappear:(BOOL)animated{
    [googleMapView stopRendering];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    if (self.pageDragView.pageIsCurled) {
        return NO;
    } else {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    }
}

- (BOOL)shouldAutorotate
{
    return !self.pageDragView.pageIsCurled;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    [self.pageDragView refreshPageCurlView];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    [self checkOrientation];
}

- (void)checkOrientation{
    snapPoint = [self.pageDragView.pageCurlView.snappingPoints objectAtIndex:0];
    
    if ([[UIDevice currentDevice] orientation]==UIInterfaceOrientationLandscapeRight||[[UIDevice currentDevice] orientation]==UIInterfaceOrientationLandscapeLeft) {
        [snapPoint setRadius:rCurlLandscape];
    }else{
        [snapPoint setRadius:rCurlPortrait];
    }
}

- (IBAction)quitCurl:(id)sender {
    if (self.pageDragView.pageIsCurled) {
        [self.pageDragView uncurlPageAnimated:YES completion:nil];
    }
}
@end

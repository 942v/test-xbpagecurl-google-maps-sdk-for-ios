//
//  ViewController.h
//  Test-XBCurlView+GoogleMapsSDKiOS
//
//  Created by Guillermo Saenz on 5/15/13.
//  Copyright (c) 2013 LaHacemos. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <GoogleMaps/GoogleMaps.h>

#import "XBPageDragView.h"

@interface ViewController : UIViewController <GMSMapViewDelegate>{
    IBOutlet UIView *frontView;
    
    XBSnappingPoint *snapPoint;
}
@property (weak, nonatomic) IBOutlet XBPageDragView *pageDragView;
- (IBAction)quitCurl:(id)sender;

@end

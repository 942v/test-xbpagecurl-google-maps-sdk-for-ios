//
//  GoogleAPIKey.h
//  Test-XBCurlView+GoogleMapsSDKiOS
//
//  Created by Guillermo Saenz on 5/15/13.
//  Copyright (c) 2013 LaHacemos. All rights reserved.
//


// Get your API key (https://code.google.com/apis/console/) for the Bundle Indentifier: com.LH.TestXBCurlViewGoogleMapsiOS
const static NSString *GoogleAPIKey = @"";